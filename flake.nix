# flake.nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
	common-pkgs = pkgs: (with pkgs; [
	    libudev-zero
	    libusb
            git
            wget
            gnumake

            flex
            bison
            gperf
            pkg-config

            cmake
	    ccache
	    libffi
	    openssl
	    dfu-util
	    libz

            ncurses5

            ninja


	    platformio
	    esptool

	]);
	# Must not use pythonX.withPackages here: Created venv will conflict with Espressif's venv
	pio-python-pkgs = pkgs: [ (pkgs.python3.withPackages (p: with p; [ pip setuptools pyserial ])) ];
	idf-python-pkgs = pkgs: with pkgs; [
	  python3
          python3Packages.pip
	  python3Packages.virtualenv
	  python3Packages.pyserial
	];
      in rec
      {
        devShells.idf = (pkgs.buildFHSUserEnv rec {
          name = "ESP-IDF Dev Shell";
          targetPkgs = pkgs: (common-pkgs pkgs) ++ (idf-python-pkgs pkgs) ++ [(pkgs.callPackage ./esp32-toolchain.nix {})];
           #shellHook = ''
            #export IDF_PATH=$(pwd)/esp-idf
            #export PATH=$IDF_PATH/tools:$PATH
            #export IDF_PYTHON_ENV_PATH=$(pwd)/.python_env

            #if [ ! -e $IDF_PYTHON_ENV_PATH ]; then
            #  python -m venv $IDF_PYTHON_ENV_PATH
            #  . $IDF_PYTHON_ENV_PATH/bin/activate
            #  pip install -r $IDF_PATH/requirements.txt
            #else
            #  . $IDF_PYTHON_ENV_PATH/bin/activate
            #fi

	    #$IDF_PATH/install.fish

	    #echo Hi and welcome to my IDF shell!
          #'';
	  runScript = "fish";
        }).env;

	devShells.default = devShells.idf;
	
        devShells.pio = (pkgs.buildFHSUserEnv rec {
          name = "ESP PIO Dev Shell";
          targetPkgs = pkgs: (common-pkgs pkgs) ++ (pio-python-pkgs pkgs);
	  runScript = "fish";
        }).env;
      }
    );
}

